import socket

# Primero creamos el socket
# Siempre utilizaremos esos parámetros:
#  tipo de socket: AF_INET y SOCK_STREAM
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Obtener informacion sobre el socket creado
print()
print("Socket creado:")
print(s)